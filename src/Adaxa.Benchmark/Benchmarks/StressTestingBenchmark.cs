﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Loggers;

using Newtonsoft.Json;

using Adaxa.Api;
using Adaxa.Api.Dto;

namespace Adaxa.Benchmark
{
    public class StressTestingConfig : ManualConfig
    {
        public StressTestingConfig()
        {
            Add(CsvMeasurementsExporter.Default);
            Add(RPlotExporter.Default);
        }
    }

    [MemoryDiagnoser]
    [Config(typeof(StressTestingConfig))]
    [RPlotExporter]
    [SimpleJob(RunStrategy.ColdStart, targetCount: 1, invocationCount: 1)]
    public class StressTestingBenchmark
    {
        private const string BaseUrl = "http://dev-win.mercury.adaxatech.com"; // "http://192.168.24.22"; // "https://localhost:44341";

        private AdaxaClient _client;
        private ILogger _logger;

        public StressTestingBenchmark() { }

        [GlobalSetup]
        public void GlobalSetup()
        {
            _client = AdaxaClientFactory.CreateAuditorClientAsync(BaseUrl)
                .GetAwaiter()
                .GetResult();

            _logger = ConsoleLogger.Default;
        }

        [GlobalCleanup]
        public void GlobalCleanup()
        {
            _client?.Dispose();
        }

        [Benchmark]
        public async Task GetFacilitySearchResponseTime()
        {
            var searchStrings = new[]
            {
                "sssss", "query", "track", "cnt", ""
            };

            var searchTasks = new List<Task<PageDto<ShortFacilityDetailsDto>>>();

            foreach (var searchString in searchStrings)
            {
                var searchParams = new SearchFacilitiesDto
                {
                    SearchString = searchString,
                    SortingType = FacilitySortingType.ById,
                    SortingOrder = SortingOrder.Ascending
                };

                _logger.WriteInfo($"Start facilities search with search string = '{searchString}'\r\n");

                var searchTask = _client.Facilities.SearchAsync(searchParams);

                searchTasks.Add(searchTask);
            }

            try
            {
                var searchResults = await Task.WhenAll(searchTasks);

                _logger.WriteInfo("Search completed\r\n");

                var resultsText = JsonConvert.SerializeObject(new
                {
                    Results = searchResults
                }, Formatting.Indented);

                _logger.WriteInfo(resultsText);
                _logger.Write("\r\n");
            }
            catch (AggregateException aggExc)
            {
                foreach (var innerExc in aggExc.InnerExceptions)
                {
                    _logger.WriteError(innerExc.ToString());
                    _logger.Write("\r\n");
                }
                _logger.Write("\r\n");
            }
            catch (Exception exc)
            {
                _logger.WriteError(exc.ToString());
                _logger.Write("\r\n");
            }
        }
    }
}