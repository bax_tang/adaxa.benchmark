﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api;
using Adaxa.Api.Dto;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Loggers;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Adaxa.Benchmark
{
    public class AuditorInteractionConfig : ManualConfig
    {
        public AuditorInteractionConfig()
        {
            Add(CsvMeasurementsExporter.Default);
            Add(RPlotExporter.Default);
        }
    }

    [Config(typeof(AuditorInteractionConfig))]
    [MemoryDiagnoser]
    [RPlotExporter]
    [SimpleJob(RunStrategy.ColdStart, targetCount: 1, invocationCount: 1)]
    public class AuditorInteractionBenchmark
    {
        private const string BaseUrl = "http://dev-win.mercury.adaxatech.com"; // "http://192.168.24.22"; // "https://localhost:44341";

        private ILogger _logger;
        private JsonSerializerSettings _serializerSettings;

        public AuditorInteractionBenchmark() { }

        [GlobalSetup]
        public void GlobalSetup()
        {
            _logger = ConsoleLogger.Default;

            var serializerSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            serializerSettings.Converters.Add(new StringEnumConverter(new DefaultNamingStrategy()));

            _serializerSettings = serializerSettings;
        }

        [GlobalCleanup]
        public void GlobalCleanup()
        {
        }

        [Benchmark]
        public async Task ExecuteInteraction()
        {
            var rnd = new Random();

            var auditorAccounts = AccountService.GetUserAccounts(ServerType.QA, AccountType.Auditor);
            var facilitySearchStrings = GetFacilitySearchStrings();

            var tasksList = new Task[auditorAccounts.Count];

            for (int idx = 0; idx < auditorAccounts.Count; ++idx)
            {
                var (AccountName, Password) = auditorAccounts[idx];

                var executionTask = ProceedAuditorInteractionAsync(AccountName, Password, facilitySearchStrings, rnd);

                tasksList[idx] = executionTask;
            }

            await Task.WhenAll(tasksList);
        }

        private List<string> GetFacilitySearchStrings()
        {
            var searchStrings = new List<string>
            {
                "kerr",
                "aud",
                "seq",
                "dat",
                "new",
                "stop",
                "ocl",
                "step"
            };

            return searchStrings;
        }

        private async Task ProceedAuditorInteractionAsync(string accountName, string password, List<string> facilitySearchStrings, Random rnd)
        {
            await Task.Delay(100 * rnd.Next(5, 10));

            var client = await AdaxaClientFactory.CreateAuditorClientAsync(BaseUrl, accountName, password);

            var getFormsRequest = new PaginatedRequestDto
            {
                Page = 1,
                Size = rnd.Next(5, 10)
            };

            var formsList = await client.Forms.GetMobileListAsync(getFormsRequest);
            if (formsList.Data.Count <= 0)
            {
                _logger.WriteInfo($"There is no forms for account '{accountName}'\r\n");
                return;
            }

            var formIndex = rnd.Next(0, formsList.Data.Count);
            var formDetails = formsList.Data[formIndex];

            LogObjectAsJson("Random selected form:", formDetails);

            var formWithQuestions = await client.Forms.GetMobileAsync(formDetails.Id);

            var searchStringIndex = rnd.Next(0, facilitySearchStrings.Count);
            var searchString = facilitySearchStrings[searchStringIndex];

            var searchFacilitiesRequest = new SearchFacilitiesDto
            {
                Page = 1,
                Size = rnd.Next(5, 20),
                SortingOrder = SortingOrder.Ascending,
                SortingType = FacilitySortingType.ById,
                SearchString = searchString
            };

            var facilitiesList = await client.Facilities.SearchAsync(searchFacilitiesRequest);
            if (facilitiesList.Data.Count <= 0)
            {
                _logger.WriteInfo($"There is no facilities found for account '{accountName}'\r\n");
                return;
            }

            var facilityIndex = rnd.Next(0, facilitiesList.Data.Count);
            var facilityDetails = facilitiesList.Data[facilityIndex];

            LogObjectAsJson("Random selected facility:", facilityDetails);

            var facilityInfo = await client.Facilities.GetByIdAsync(facilityDetails.Id);

            var createSurveyRequest = await GenerateSurveyRequestAsync(client, formWithQuestions, facilityInfo, rnd);

            var newSurvey = await client.Surveys.CreateAsync(createSurveyRequest);
            var reportRegistration = await client.Surveys.CreateAuditorReportAsync(newSurvey.Id);

            LogObjectAsJson("New survey created:", newSurvey);
            LogObjectAsJson("Survey report registration response:", reportRegistration);

            #region Excluded code
            /*
            do
            {
                await Task.Delay(2000);

                try
                {
                    var surveyReport = await client.Surveys.GetAuditorReportAsync(newSurvey.Id);
                    if (surveyReport != null)
                    {
                        LogObjectAsJson("Report that was generated for survey:", surveyReport);
                        break;
                    }
                }
                catch (AdaxaException adaxaExc)
                {
                    switch (adaxaExc.StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                            continue;
                    }
                }
                catch
                {
                    throw;
                }
            }
            while (true);
            */
            #endregion

            client.Dispose();
        }

        private async Task<PhotoDetailsDto> CreateFileAsync(AdaxaClient client, Stream imageStream)
        {
            var contentType = "image/jpeg";

            var urlRequest = new CreatePreSignedUrlsDto
            {
                Files = new List<CreatePreSignedUrlItemDto>
                {
                    new CreatePreSignedUrlItemDto
                    {
                        ContentType = contentType,
                        Type = FileType.Image
                    }
                }
            };

            var preSignedUrlsList = await client.Files.CreatePreSignedUrlsAsync(urlRequest);

            var fileInfo = preSignedUrlsList[0];

            return await client.Files.UploadAsync(fileInfo, contentType, imageStream);
        }

        private async Task<CreateSurveyDto> GenerateSurveyRequestAsync(AdaxaClient client, FormDetailsWithQuestionsDto form, FacilityDetailsDto facility, Random rnd)
        {
            var filePath = @"E:\Downloads\Images\14-13-29.jpg";

            var facilitySignatureImageStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            var facilitySignature = await CreateFileAsync(client, facilitySignatureImageStream);

            var mcnaSignatureImageStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            var mcnaSignature = await CreateFileAsync(client, mcnaSignatureImageStream);

            var nowTime = DateTimeOffset.Now;

            var createSurveyRequest = new CreateSurveyDto
            {
                Comment = "some comment text",
                Date = nowTime,
                Duration = rnd.Next(0, 50),
                FacilityId = facility.Id,
                FormId = form.Id,
                FacilitySignatureId = facilitySignature.Id,
                FacilitySignatureEmail = "facility_signature@mcna.com",
                FacilitySignatureName = "facility_signature",
                McnaSignatureId = mcnaSignature.Id,
                McnaSignatureEmail = "mcna_signature@mcna.com",
                McnaSignatureName = "mcna_signature",
                Score = string.Empty
            };

            var surveyQuestions = new List<CreateSurveyQuestionDto>();

            var directQuestions = form.Questions.Where(x => x.Type != FormQuestionType.Compound);
            foreach (var question in directQuestions)
            {
                var surveyQuestion = new CreateSurveyQuestionDto
                {
                    Id = question.Id,
                    Answer = new CreateSurveyQuestionAnswerDto
                    {
                        Comment = $"some answer comment for question with id '{question.Id}'",
                        Data = new
                        {
                            Value = "some data for answer"
                        },
                        PhotosIds = new List<Guid>()
                    }
                };

                surveyQuestions.Add(surveyQuestion);
            }

            createSurveyRequest.Questions = surveyQuestions;

            return createSurveyRequest;
        }

        private void LogObjectAsJson<TInstance>(string prefix, TInstance instance)
        {
            var instanceJson = JsonConvert.SerializeObject(instance, _serializerSettings);

            _logger.WriteInfo(string.Concat(prefix, "\r\n", instanceJson, "\r\n"));
        }
    }
}