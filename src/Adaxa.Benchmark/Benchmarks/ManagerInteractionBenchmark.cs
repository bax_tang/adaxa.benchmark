﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api;
using Adaxa.Api.Dto;

using BenchmarkDotNet;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Running;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Adaxa.Benchmark
{
    public class ManagerInteractionConfig : ManualConfig
    {
        public ManagerInteractionConfig()
        {
            Add(Job.Dry
                .With(Platform.X64)
                .With(Jit.RyuJit)
                .With(Runtime.Core)
                .With(RunStrategy.ColdStart)
                .RunOncePerIteration()
                .WithLaunchCount(1)
                .WithIterationCount(1)
                .WithInvocationCount(1));

            Add(CsvMeasurementsExporter.Default);
            Add(RPlotExporter.Default);
        }
    }

    [Config(typeof(ManagerInteractionConfig))]
    [MemoryDiagnoser]
    [RPlotExporter]
    public class ManagerInteractionBenchmark
    {
        private const string BaseUrl = "http://dev-win.mercury.adaxatech.com"; // "http://192.168.24.22"; // "https://localhost:44341";

        private ILogger _logger;
        private JsonSerializerSettings _serializerSettings;

        public ManagerInteractionBenchmark() { }

        [GlobalSetup]
        public void GlobalSetup()
        {
            _logger = ConsoleLogger.Default;

            var serializerSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            serializerSettings.Converters.Add(new StringEnumConverter(new DefaultNamingStrategy()));

            _serializerSettings = serializerSettings;
        }

        [GlobalCleanup]
        public void GlobalCleanup()
        {
        }

        [Benchmark]
        public async Task ExecuteInteraction()
        {
            var rnd = new Random();

            var managerAccounts = AccountService.GetUserAccounts(ServerType.QA, AccountType.Manager);

            var tasksList = new Task[managerAccounts.Count];

            for (int idx = 0; idx < managerAccounts.Count; ++idx)
            {
                var (AccountName, Password) = managerAccounts[idx];

                var executionTask = ProceedManagerInteractionAsync(AccountName, Password, rnd);

                tasksList[idx] = executionTask;
            }

            await Task.WhenAll(tasksList);
        }

        private async Task ProceedManagerInteractionAsync(string accountName, string password, Random rnd)
        {
            await Task.Delay(100 * rnd.Next(5, 10));

            var client = await AdaxaClientFactory.CreateManagerClientAsync(BaseUrl, accountName, password);

            var auditors = await client.Users.GetAuditorsAsync();
            _logger.WriteInfo($"There is {auditors.Count} auditors linked for current user.\r\n");

            var getSurveysRequest = new GetManagerSurveysRequestDto
            {
                Page = 1,
                Size = rnd.Next(5, 20)
            };

            var surveys = await client.Surveys.GetListByManagerAsync(getSurveysRequest);

            var surveysMsg = string.Format("There is {0} surveys for current manager; last date is {1}.\r\n",
                surveys.Page.PaginationContext.TotalItems,
                (surveys.LastDate != null) ? surveys.LastDate.Value.ToString() : "<null>");
            _logger.WriteInfo(surveysMsg);

            var getFormsRequest = new GetFormsListDto
            {
                FormStatus = FormStatus.Draft,
                Page = 1,
                Size = rnd.Next(5, 20)
            };

            var formsList = await client.Forms.GetListAsync(getFormsRequest);
            if (formsList.Data.Count <= 0)
            {
                _logger.WriteInfo("There is no forms for current manager.\r\n");
                return;
            }

            var formsTotalCount = formsList.PaginationContext.TotalItems;

            var formIndex = rnd.Next(0, formsList.Data.Count);
            var formInfo = formsList.Data[formIndex];

            _logger.WriteInfo($"Selected existing form with Id = '{formInfo.Id}'.\r\n");

            var fullFormInfo = await client.Forms.GetAsync(formInfo.Id);

            var updateFormDto = MakeUpdateDraftFormDtoFromDetails(fullFormInfo, "NewName100500");
            var updateResult = await client.Forms.UpdateDraftAsync(updateFormDto);
            if (updateResult != null)
            {
                _logger.WriteInfo("Existing form updated successfully.\r\n");
            }

            var createFormDto = MakeCreateFormDto();
            var newForm = await client.Forms.CreateAsync(createFormDto);
            if (newForm != null)
            {
                _logger.WriteInfo($"New form created successfully; form id is '{newForm.Id}'.\r\n");
            }

            var getUpdatedFormsListRequest = new GetFormsListDto
            {
                FormStatus = FormStatus.Draft,
                Page = 1,
                Size = formsTotalCount + 1
            };

            var updatedFormsList = await client.Forms.GetListAsync(getUpdatedFormsListRequest);
            if (updatedFormsList.Data.Any(x => x.Id == newForm.Id))
            {
                _logger.WriteInfo("New form correctly created and returns using GET /api/v1/forms endpoint.\r\n");
            }

            client.Dispose();
        }

        private UpdateDraftFormDto MakeUpdateDraftFormDtoFromDetails(FormDetailsWithQuestionsDto formDto, string newName)
        {
            var result = new UpdateDraftFormDto
            {
                Id = formDto.Id,
                Name = string.Concat(newName, "; old name: ", formDto.Name),
                Type = formDto.Type
            };

            var updateQuestions = new List<UpdateDraftFormQuestionDto>(formDto.Questions.Count);

            foreach (var question in formDto.Questions)
            {
                var updateQuestion = new UpdateDraftFormQuestionDto
                {
                    Id = question.Id,
                    CompoundQuestionId = question.CompoundQuestionId,
                    Data = question.Data,
                    IsPhotoAvailable = question.IsPhotoAvailable,
                    Order = question.Order,
                    RenderAnswerClientId = question.RenderCondition?.AnswerId,
                    RenderQuestionId = question.RenderCondition?.QuestionId,
                    Title = question.Title,
                    Type = question.Type
                };

                updateQuestions.Add(updateQuestion);
            }

            result.Questions = updateQuestions;

            return result;
        }

        private CreateFormDto MakeCreateFormDto()
        {
            var formName = "NewFormName_" + Guid.NewGuid().ToString("D").ToUpperInvariant();

            var createFormDto = new CreateFormDto
            {
                Name = formName,
                Type = FormType.SiteAuditTool
            };

            var questions = new List<CreateFormQuestionDto>();

            for (var idx = 0; idx < 5; ++idx)
            {
                var formQuestion = new CreateFormQuestionDto
                {
                    Id = Guid.NewGuid(),
                    Title = "NewFormQuestion_" + idx,
                    Order = idx,
                    Data = new object(),
                    IsPhotoAvailable = false,
                    Type = FormQuestionType.TextInput
                };

                questions.Add(formQuestion);
            }

            createFormDto.Questions = questions;

            return createFormDto;
        }

        private void LogObjectAsJson<TInstance>(string prefix, TInstance instance)
        {
            var instanceJson = JsonConvert.SerializeObject(instance, _serializerSettings);

            _logger.WriteInfo(string.Concat(prefix, "\r\n", instanceJson, "\r\n"));
        }
    }
}