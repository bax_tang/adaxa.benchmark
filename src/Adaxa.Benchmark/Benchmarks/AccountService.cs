﻿using System;
using System.Collections.Generic;

namespace Adaxa.Benchmark
{
    public enum ServerType
    {
        Unknown = 0,
        Local = 1,
        Dev = 2,
        QA = 3
    }

    public enum AccountType
    {
        Unknown = 0,
        Auditor = 1,
        Manager = 2
    }

    public static class AccountService
    {
        public static List<(string AccountName, string Password)> GetUserAccounts(ServerType serverType, AccountType accountType)
        {
            switch (serverType)
            {
                case ServerType.Dev:
                case ServerType.Local:

                    switch (accountType)
                    {
                        case AccountType.Auditor:
                            return GetLocalDevAuditorAccounts();

                        case AccountType.Manager:
                            return GetLocalDevManagerAccounts();

                        default:
                            throw new ArgumentOutOfRangeException(nameof(accountType), "Unknown account type.");
                    }

                case ServerType.QA:
                    switch (accountType)
                    {
                        case AccountType.Auditor:
                            return GetQAAuditorAccounts();

                        case AccountType.Manager:
                            return GetQAManagerAccounts();

                        default:
                            throw new ArgumentOutOfRangeException(nameof(accountType), "Unknown account type.");
                    }

                default:
                    throw new ArgumentOutOfRangeException(nameof(serverType), "Unknown server type.");
            }
        }

        private static List<(string AccountName, string Password)> GetLocalDevAuditorAccounts()
        {
            var auditorAccounts = new List<(string AccountName, string Password)>
            {
                ("sattestflaud@mcna.net", "pwd"),
                ("sattestlaaud@mcna.net", "pwd")
            };

            return auditorAccounts;
        }

        private static List<(string AccountName, string Password)> GetLocalDevManagerAccounts()
        {
            var managerAccounts = new List<(string AccountName, string Password)>
            {
                ("sattestflman@mcna.net", "pwd"),
                ("sattesttxman@mcna.net", "pwd")
            };

            return managerAccounts;
        }

        private static List<(string AccountName, string Password)> GetQAAuditorAccounts()
        {
            var auditorAccounts = new List<(string AccountName, string Password)>
            {
                ("SATTEST05UPD@mcna.net", "Sunkern2019!"),
                ("SATTEST13@mcna.net", "Dewgong2019!"),
                ("SATTEST14@mcna.net", "Florges2019!")
            };

            return auditorAccounts;
        }

        private static List<(string AccountName, string Password)> GetQAManagerAccounts()
        {
            var managerAccounts = new List<(string AccountName, string Password)>
            {
                ("SATTEST01@mcna.net", "Candypink2019!"),
                ("SATTEST02@mcna.net", "Sudowoodo2019!"),
                ("SATTEST16@mcna.net", "Hippopotas2019!"),
                ("SATTEST05UPD@mcna.net", "Sunkern2019!")
            };

            return managerAccounts;
        }
    }
}