﻿using System;

using BenchmarkDotNet;
using BenchmarkDotNet.Running;

namespace Adaxa.Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<AuditorInteractionBenchmark>();
        }
    }
}
