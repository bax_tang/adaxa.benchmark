``` ini

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.18362
Intel Core i7-4770 CPU 3.40GHz (Haswell), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=2.2.402
  [Host] : .NET Core 2.2.7 (CoreCLR 4.6.28008.02, CoreFX 4.6.28008.03), 64bit RyuJIT
  Dry    : .NET Core 2.2.7 (CoreCLR 4.6.28008.02, CoreFX 4.6.28008.03), 64bit RyuJIT

Job=Dry  Jit=RyuJit  Platform=X64  
Runtime=Core  InvocationCount=1  IterationCount=1  
LaunchCount=1  RunStrategy=ColdStart  UnrollFactor=1  
WarmupCount=1  

```
|             Method |    Mean | Error | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |--------:|------:|------:|------:|------:|----------:|
| ExecuteInteraction | 4.789 s |    NA |     - |     - |     - |   2.42 KB |
