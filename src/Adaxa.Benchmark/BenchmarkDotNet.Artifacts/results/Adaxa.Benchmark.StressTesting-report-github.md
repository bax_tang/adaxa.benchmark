``` ini

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.17134.1006 (1803/April2018Update/Redstone4)
Intel Core i7-4770 CPU 3.40GHz (Haswell), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=2.2.402
  [Host]     : .NET Core 2.2.7 (CoreCLR 4.6.28008.02, CoreFX 4.6.28008.03), 64bit RyuJIT
  Job-WNVVDL : .NET Core 2.2.7 (CoreCLR 4.6.28008.02, CoreFX 4.6.28008.03), 64bit RyuJIT

IterationCount=1  RunStrategy=ColdStart  

```
|                        Method |     Mean | Error | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------ |---------:|------:|------:|------:|------:|----------:|
| GetFacilitySearchResponseTime | 635.4 ms |    NA |     - |     - |     - |  117.8 KB |
