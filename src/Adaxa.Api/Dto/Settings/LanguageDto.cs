﻿using System;

namespace Adaxa.Api.Dto
{
    public class LanguageDto
    {
        public string Short { get; set; }

        public string Full { get; set; }
    }
}