﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Adaxa.Api.Dto
{
    public class ServerResponseDto
    {
        public HttpStatusCode StatusCode { get; set; }

        public HttpContent ResponseContent { get; set; }

        public HttpResponseHeaders ResponseHeaders { get; set; }

        internal ServerResponseDto() { }

        internal static ServerResponseDto FromResponseMessage(HttpResponseMessage responseMessage)
        {
            var response = new ServerResponseDto
            {
                StatusCode = responseMessage.StatusCode,
                ResponseContent = responseMessage.Content,
                ResponseHeaders = responseMessage.Headers
            };
            return response;
        }
    }
}