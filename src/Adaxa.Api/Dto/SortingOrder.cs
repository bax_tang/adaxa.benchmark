﻿using System;

namespace Adaxa.Api.Dto
{
    public enum SortingOrder
    {
        Descending = 0,
        Ascending = 1
    }
}