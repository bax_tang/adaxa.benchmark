﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class SurveyQuestionAnswerDetailsDto
    {
        public Guid Id { get; set; }

        public object Data { get; set; }

        public string Comment { get; set; }

        public List<PhotoDetailsDto> Photos { get; set; }
    }
}