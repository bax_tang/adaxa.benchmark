﻿using System;

namespace Adaxa.Api.Dto
{
    public class SurveyReportDetailsDto
    {
        public string FileName { get; set; }

        public string ReportUrl { get; set; }
    }
}