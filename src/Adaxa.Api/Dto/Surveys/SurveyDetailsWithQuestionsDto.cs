﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class SurveyDetailsWithQuestionsDto
    {
        public Guid Id { get; set; }

        public Guid FacilityId { get; set; }

        public Guid FormId { get; set; }

        public long AuditorId { get; set; }

        public DateTimeOffset Date { get; set; }

        public Guid FacilitySignatureId { get; set; }

        public string FacilitySignatureName { get; set; }

        public string FacilitySignatureEmail { get; set; }

        public string FacilitySignatureUrl { get; set; }

        public Guid McnaSignatureId { get; set; }

        public string McnaSignatureName { get; set; }

        public string McnaSignatureEmail { get; set; }

        public string McnaSignatureUrl { get; set; }

        public bool? HasNote { get; set; }

        public string Comment { get; set; }

        public long Duration { get; set; }

        public string Score { get; set; }

        public List<SurveyQuestionDetailsDto> Questions { get; set; }
    }
}