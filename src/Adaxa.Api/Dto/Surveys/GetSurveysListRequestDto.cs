﻿using System;

namespace Adaxa.Api.Dto
{
    public class GetAuditorSurveysRequestDto : PaginatedRequestDto { }

    public class GetManagerSurveysRequestDto : PaginatedRequestDto
    {
        public long? AuditorId { get; set; }
    }
}