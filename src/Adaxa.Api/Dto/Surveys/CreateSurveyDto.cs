﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class CreateSurveyDto
    {
        public Guid FormId { get; set; }

        public Guid FacilityId { get; set; }

        public DateTimeOffset Date { get; set; }

        public long Duration { get; set; }

        public string FacilitySignatureName { get; set; }

        public string FacilitySignatureEmail { get; set; }

        public Guid FacilitySignatureId { get; set; }

        public string McnaSignatureName { get; set; }

        public string McnaSignatureEmail { get; set; }

        public Guid McnaSignatureId { get; set; }

        public string Comment { get; set; }

        public string Score { get; set; }

        public List<CreateSurveyQuestionDto> Questions { get; set; }
    }
}