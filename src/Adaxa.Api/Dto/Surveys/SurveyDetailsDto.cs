﻿using System;

namespace Adaxa.Api.Dto
{
    public class SurveyDetailsDto
    {
        public Guid SurveyId { get; set; }

        public long Duration { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public string Auditor { get; set; }

        public FormType FormType { get; set; }

        public bool IsNewSurvey { get; set; }

        public bool HasNote { get; set; }

        public bool IsReportExist { get; set; }

        public bool IsReportGenerating { get; set; }

        public SubmissionFacilityDetailsDto Facility { get; set; }
    }
}