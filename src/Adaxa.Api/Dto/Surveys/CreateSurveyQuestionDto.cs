﻿using System;

namespace Adaxa.Api.Dto
{
    public class CreateSurveyQuestionDto
    {
        public Guid Id { get; set; }

        public CreateSurveyQuestionAnswerDto Answer { get; set; }
    }
}