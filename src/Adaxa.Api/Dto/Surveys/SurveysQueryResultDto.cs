﻿using System;

namespace Adaxa.Api.Dto
{
    public class SurveysQueryResultDto
    {
        public PageDto<SurveyDetailsDto> Page { get; set; }
    }

    public class AuditorSurveysQueryResultDto : SurveysQueryResultDto { }

    public class ManagerSurveysQueryResultDto : SurveysQueryResultDto
    {
        public DateTimeOffset? LastDate { get; set; }
    }
}