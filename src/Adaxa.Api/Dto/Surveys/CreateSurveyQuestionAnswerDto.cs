﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class CreateSurveyQuestionAnswerDto
    {
        public object Data { get; set; }

        public List<Guid> PhotosIds { get; set; }

        public string Comment { get; set; }
    }
}