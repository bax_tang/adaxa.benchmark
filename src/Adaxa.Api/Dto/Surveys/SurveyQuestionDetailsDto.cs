﻿using System;

namespace Adaxa.Api.Dto
{
    public class SurveyQuestionDetailsDto
    {
        public Guid Id { get; set; }

        public SurveyQuestionAnswerDetailsDto Answer { get; set; }
    }
}