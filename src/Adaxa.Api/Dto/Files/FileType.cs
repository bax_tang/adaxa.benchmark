﻿using System;

namespace Adaxa.Api.Dto
{
    public enum FileType
    {
        Image,
        Report
    }
}