﻿using System;

namespace Adaxa.Api.Dto
{
    public class PhotoDetailsDto
    {
        public Guid Id { get; set; }

        public string ObjectKey { get; set; }

        public string Url { get; set; }
    }
}