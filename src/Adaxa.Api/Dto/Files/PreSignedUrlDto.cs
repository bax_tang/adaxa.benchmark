﻿using System;

namespace Adaxa.Api.Dto
{
    public class PreSignedUrlDto
    {
        public Guid Id { get; set; }

        public string PreSignedUrl { get; set; }

        public string ObjectKey { get; set; }

        public string ContentType { get; set; }
    }
}