﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class CreatePreSignedUrlsDto
    {
        public List<CreatePreSignedUrlItemDto> Files { get; set; }
    }
}