﻿using System;

namespace Adaxa.Api.Dto
{
    public class CreatePreSignedUrlItemDto
    {
        public FileType Type { get; set; }

        public string ContentType { get; set; }
    }
}