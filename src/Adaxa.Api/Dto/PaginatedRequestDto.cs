﻿using System;

namespace Adaxa.Api.Dto
{
    public class PaginatedRequestDto
    {
        public int? Page { get; set; }

        public int? Size { get; set; }
    }
}