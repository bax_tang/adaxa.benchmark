﻿using System;

namespace Adaxa.Api.Dto
{
    public enum FacilitySortingType
    {
        ByName = 0,
        ById,
        ByAddress,
        ByCity,
        ByState
    }
}