﻿using System;

namespace Adaxa.Api.Dto
{
    public class SubmissionFacilityDetailsDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
    }
}