﻿using System;

namespace Adaxa.Api.Dto
{
    public class ShortFacilityDetailsDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }
    }
}