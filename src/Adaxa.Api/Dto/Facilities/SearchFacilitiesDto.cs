﻿using System;

namespace Adaxa.Api.Dto
{
    public class SearchFacilitiesDto : PaginatedRequestDto
    {
        public string SearchString { get; set; }

        public FacilitySortingType? SortingType { get; set; }

        public SortingOrder? SortingOrder { get; set; }
    }
}