﻿using System;

namespace Adaxa.Api.Dto
{
    public class FacilityDetailsDto
    {
        public Guid Id { get; set; }

        public long ExternalId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Phone { get; set; }

        public string Npi { get; set; }

        public string Type { get; set; }

        public DateTimeOffset? LastAuditDate { get; set; }

        public DateTimeOffset? MemberSinceDate { get; set; }

        public string LastScore { get; set; }

        public string Medicaid { get; set; }
    }
}