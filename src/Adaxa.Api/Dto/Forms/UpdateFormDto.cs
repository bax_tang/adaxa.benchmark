﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class UpdateFormDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public List<UpdateFormQuestionDto> Questions { get; set; }
    }
}