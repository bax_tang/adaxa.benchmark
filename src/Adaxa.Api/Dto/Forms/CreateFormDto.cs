﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class CreateFormDto
    {
        public string Name { get; set; }

        public FormType Type { get; set; }

        public List<CreateFormQuestionDto> Questions { get; set; }
    }
}