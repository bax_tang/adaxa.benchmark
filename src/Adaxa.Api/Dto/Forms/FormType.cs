﻿using System;

namespace Adaxa.Api.Dto
{
    public enum FormType
    {
        SiteAuditTool = 0,
        ProviderSatisfactionSurvey,
        AppointmentAvailabilitySurvey,
        ProviderSiteContactForm,
        ProviderOrientationTraining,
        AdditionalPersonnelTraining,
        ProviderEducationTraining,
        DentalRecordsAudit
    }
}