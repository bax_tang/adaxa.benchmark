﻿using System;

namespace Adaxa.Api.Dto
{
    public class UpdateFormStatusDto
    {
        public FormStatus Status { get; set; }

        public string FormName { get; set; }
    }
}