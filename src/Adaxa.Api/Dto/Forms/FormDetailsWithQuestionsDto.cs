﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class FormDetailsWithQuestionsDto : FormDetailsDto
    {
        public List<FormQuestionDetailsDto> Questions { get; set; }
    }
}