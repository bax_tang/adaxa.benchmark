﻿using System;

namespace Adaxa.Api.Dto
{
    public class CreateFormQuestionDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public FormQuestionType Type { get; set; }

        public Guid? CompountQuestionId { get; set; }

        public int Order { get; set; }

        public object Data { get; set; }

        public Guid? RenderQuestionId { get; set; }

        public Guid? RenderAnswerClientId { get; set; }

        public bool IsPhotoAvailable { get; set; }
    }
}