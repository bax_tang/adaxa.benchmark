﻿using System;

namespace Adaxa.Api.Dto
{
    public class FormDetailsDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public FormStatus Status { get; set; }

        public FormType Type { get; set; }

        public DateTimeOffset CreatedDate { get; set; }
    }
}