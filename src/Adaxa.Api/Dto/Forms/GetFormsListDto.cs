﻿using System;

namespace Adaxa.Api.Dto
{
    public class GetFormsListDto : PaginatedRequestDto
    {
        public FormStatus? FormStatus { get; set; }
    }
}