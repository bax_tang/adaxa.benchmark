﻿using System;

namespace Adaxa.Api.Dto
{
    public class UpdateFormQuestionDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public FormQuestionType Type { get; set; }

        public Guid? CompoundQuestionId { get; set; }

        public object Data { get; set; }
    }
}