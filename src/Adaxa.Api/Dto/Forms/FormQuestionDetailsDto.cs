﻿using System;

namespace Adaxa.Api.Dto
{
    public class FormQuestionDetailsDto
    {
        public Guid Id { get; set; }

        public Guid FormId { get; set; }

        public string Title { get; set; }

        public FormQuestionType Type { get; set; }

        public object Data { get; set; }

        public Guid? CompoundQuestionId { get; set; }

        public int Order { get; set; }

        public bool IsPhotoAvailable { get; set; }

        public FormQuestionRenderCondition RenderCondition { get; set; }
    }
}