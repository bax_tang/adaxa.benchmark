﻿using System;

namespace Adaxa.Api.Dto
{
    public class UpdateDraftFormQuestionDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public FormQuestionType Type { get; set; }

        public object Data { get; set; }

        public Guid? CompoundQuestionId { get; set; }

        public Guid? RenderQuestionId { get; set; }

        public Guid? RenderAnswerClientId { get; set; }

        public int Order { get; set; }

        public bool IsPhotoAvailable { get; set; }
    }
}