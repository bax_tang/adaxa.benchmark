﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class UpdateDraftFormDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public FormType Type { get; set; }

        public List<UpdateDraftFormQuestionDto> Questions { get; set; }
    }
}