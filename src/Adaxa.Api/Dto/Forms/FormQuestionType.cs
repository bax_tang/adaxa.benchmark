﻿using System;

namespace Adaxa.Api.Dto
{
    public enum FormQuestionType
    {
        Break = 0,
        Appraisal,
        TextInput,
        SingleChoice,
        MultipleChoice,
        WorkHours,
        Date,
        PhoneNumber,
        DropDown,
        Language,
        Compound,
        OnScreenInstruction
    }
}