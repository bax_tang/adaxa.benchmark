﻿using System;

namespace Adaxa.Api.Dto
{
    public enum FormStatus
    {
        Published = 0,
        Archive,
        Draft
    }
}