﻿using System;

namespace Adaxa.Api.Dto
{
    public class FormQuestionRenderCondition
    {
        public Guid QuestionId { get; set; }

        public Guid? AnswerId { get; set; }
    }
}