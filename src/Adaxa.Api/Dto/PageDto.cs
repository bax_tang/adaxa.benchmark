﻿using System;
using System.Collections.Generic;

namespace Adaxa.Api.Dto
{
    public class PageDto<TContent>
    {
        public IList<TContent> Data { get; set; }

        public PaginationContextDto PaginationContext { get; set; }
    }
}