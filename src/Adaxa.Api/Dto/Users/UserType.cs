﻿using System;

namespace Adaxa.Api.Dto
{
    [Flags]
    public enum UserType : int
    {
        Unknown = 0,
        Auditor = 1,
        ManagerLevel4 = 2,
        ManagerLevel3 = 4,
        ManagerLevel2 = 8,
        ManagerLevel1 = 16
    }
}