﻿using System;

namespace Adaxa.Api.Dto
{
    public class ShortUserDetailsDto
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public string DisplayName { get; set; }

        public UserType Type { get; set; }
    }
}