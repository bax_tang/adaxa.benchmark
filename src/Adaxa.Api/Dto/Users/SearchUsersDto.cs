﻿using System;

namespace Adaxa.Api.Dto
{
    public class SearchUsersDto : PaginatedRequestDto
    {
        public string SearchString { get; set; }
    }
}