﻿using System;

namespace Adaxa.Api.Dto
{
    public class SubmissionUserDetailsDto
    {
        public long Id { get; set; }

        public string DisplayName { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }
    }
}