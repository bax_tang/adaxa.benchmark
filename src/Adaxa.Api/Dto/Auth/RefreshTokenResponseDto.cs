﻿using System;

namespace Adaxa.Api.Dto
{
    public class RefreshTokenResponseDto
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}