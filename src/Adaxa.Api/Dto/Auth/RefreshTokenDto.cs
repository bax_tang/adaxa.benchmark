﻿using System;

namespace Adaxa.Api.Dto
{
    public class RefreshTokenDto
    {
        public string RefreshToken { get; set; }
    }
}