﻿using System;

namespace Adaxa.Api.Dto
{
    public class SignInResponseDto
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public ShortUserDetailsDto UserDetail { get; set; }
    }
}