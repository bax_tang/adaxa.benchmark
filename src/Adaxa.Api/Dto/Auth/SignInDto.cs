﻿using System;

namespace Adaxa.Api.Dto
{
    public class SignInDto
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}