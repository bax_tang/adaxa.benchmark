﻿using System;

namespace Adaxa.Api.Dto
{
    public class PaginationContextDto
    {
        public int Page { get; set; }

        public int Size { get; set; }

        public int TotalPages { get; set; }

        public int TotalItems { get; set; }
    }
}