﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public static class AdaxaClientFactory
    {
        public static async Task<AdaxaClient> CreateAuditorClientAsync(string baseUrl, string accountName = "sattestflaud@mcna.net", string password = "pwd",
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await CreateClientAsync(baseUrl, accountName, password, true, cancellationToken);
        }

        public static async Task<AdaxaClient> CreateManagerClientAsync(string baseUrl, string accountName = "sattestlvl2@mcna.net", string password = "pwd",
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await CreateClientAsync(baseUrl, accountName, password, false, cancellationToken);
        }

        private static async Task<AdaxaClient> CreateClientAsync(string baseUrl, string accountName, string password, bool mobile,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var client = new AdaxaClient(baseUrl);

            var signInDto = new SignInDto
            {
                Email = accountName,
                Password = password
            };

            await client.Auth.SignInAsync(signInDto, mobile, cancellationToken);

            return client;
        }
    }
}