﻿using System;

namespace Adaxa.Api
{
    internal static class AdaxaEndpoints
    {
        internal static class Auth
        {
            public const string SignIn = "/api/v1/auth/signin";
            public const string MobileSignIn = "/api/mobile/v1/auth/signin";
            public const string RefreshToken = "api/v1/auth/refreshtoken";
        }

        internal static class Facilities
        {
            public const string Search = "/api/mobile/v1/facilities";
            public const string GetById = "/api/mobile/v1/facilities/{0}";
        }

        internal static class Files
        {
            public const string PreSignedUrls = "/api/mobile/v1/files/pre-signed-url";
        }

        internal static class Forms
        {
            public const string GetList = "/api/v1/forms";
            public const string GetListMobile = "/api/mobile/v1/forms";
            public const string GetById = "/api/v1/forms/{0}";
            public const string GetByIdMobile = "/api/mobile/v1/forms/{0}";
            public const string Create = "/api/v1/forms";
            public const string Update = "/api/v1/forms";
            public const string UpdateDrafts = "/api/v1/forms/drafts";
            public const string UpdateStatus = "/api/v1/forms/{0}/status";
            public const string CloneAsDraft = "/api/v1/forms/{0}/clone";
        }

        internal static class Settings
        {
            public const string Languages = "/api/mobile/v1/settings/languages";
        }

        internal static class Surveys
        {
            public const string Create = "/api/mobile/v1/surveys";

            public const string CreateAuditorReport = "/api/v1/surveys/{0}/auditor-report";
            public const string CreateManagerReport = "/api/v1/surveys/{0}/manager-report";

            public const string GetListByAuditor = "/api/v1/surveys/auditors-surveys";
            public const string GetListByManager = "/api/v1/surveys/managers-surveys";

            public const string GetAuditorReport = "/api/v1/surveys/{0}/auditor-report";
            public const string GetManagerReport = "/api/v1/surveys/{0}/manager-report";
        }

        internal static class Users
        {
            public const string GetAuditors = "/api/v1/users/auditors";
            public const string Search = "/api/v1/users/search";
        }
    }
}