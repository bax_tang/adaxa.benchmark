﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaFacilitiesManager
    {
        private readonly AdaxaClient _client;

        internal AdaxaFacilitiesManager(AdaxaClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<PageDto<ShortFacilityDetailsDto>> SearchAsync(SearchFacilitiesDto searchParameters, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(AdaxaEndpoints.Facilities.Search, searchParameters);

            return await _client.GetAsync<PageDto<ShortFacilityDetailsDto>>(requestUri, cancellationToken);
        }

        public async Task<FacilityDetailsDto> GetByIdAsync(long id, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = string.Format(AdaxaEndpoints.Facilities.GetById, id);

            return await _client.GetAsync<FacilityDetailsDto>(requestUri, cancellationToken);
        }
    }
}