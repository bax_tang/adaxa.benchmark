﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaSurveysManager
    {
        private readonly AdaxaClient _client;

        internal AdaxaSurveysManager(AdaxaClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<SurveyDetailsWithQuestionsDto> CreateAsync(CreateSurveyDto survey, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = AdaxaEndpoints.Surveys.Create;

            return await _client.PostAsync<CreateSurveyDto, SurveyDetailsWithQuestionsDto>(requestUri, survey, cancellationToken);
        }

        public async Task<ServerResponseDto> CreateAuditorReportAsync(Guid surveyId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = string.Format(AdaxaEndpoints.Surveys.CreateAuditorReport, surveyId);

            return await _client.PostAsync(requestUri, cancellationToken);
        }

        public async Task<ServerResponseDto> CreateManagerReportAsync(Guid surveyId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = string.Format(AdaxaEndpoints.Surveys.CreateManagerReport, surveyId);

            return await _client.PostAsync(requestUri, cancellationToken);
        }

        public async Task<AuditorSurveysQueryResultDto> GetListByAuditorAsync(GetAuditorSurveysRequestDto request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(AdaxaEndpoints.Surveys.GetListByAuditor, request);

            return await _client.GetAsync<AuditorSurveysQueryResultDto>(requestUri, cancellationToken);
        }

        public async Task<ManagerSurveysQueryResultDto> GetListByManagerAsync(GetManagerSurveysRequestDto request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(AdaxaEndpoints.Surveys.GetListByManager, request);

            return await _client.GetAsync<ManagerSurveysQueryResultDto>(requestUri, cancellationToken);
        }

        public async Task<SurveyReportDetailsDto> GetAuditorReportAsync(Guid surveyId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = string.Format(AdaxaEndpoints.Surveys.GetAuditorReport, surveyId);

            return await _client.GetAsync<SurveyReportDetailsDto>(requestUri, cancellationToken);
        }

        public async Task<SurveyReportDetailsDto> GetManagerReportAsync(Guid surveyId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = string.Format(AdaxaEndpoints.Surveys.GetManagerReport, surveyId);

            return await _client.GetAsync<SurveyReportDetailsDto>(requestUri, cancellationToken);
        }
    }
}