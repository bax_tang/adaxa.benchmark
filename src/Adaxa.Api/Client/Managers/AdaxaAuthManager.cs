﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaAuthManager
    {
        private readonly AdaxaClient _client;

        internal AdaxaAuthManager(AdaxaClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<SignInResponseDto> SignInAsync(SignInDto signIn, bool mobile, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = mobile ? AdaxaEndpoints.Auth.MobileSignIn : AdaxaEndpoints.Auth.SignIn;

            var result = await _client.PostAsync<SignInDto, SignInResponseDto>(requestUri, signIn, cancellationToken, false);

            _client.CurrentUser = result.UserDetail;
            _client.Authorize(result.AccessToken, result.RefreshToken);

            return result;
        }

        public async Task<RefreshTokenResponseDto> RefreshTokenAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var refreshToken = new RefreshTokenDto
            {
                RefreshToken = _client.RefreshToken
            };

            var result = await _client.PostAsync<RefreshTokenDto, RefreshTokenResponseDto>(AdaxaEndpoints.Auth.RefreshToken, refreshToken, cancellationToken, false);

            _client.Authorize(result.AccessToken, result.RefreshToken);

            return result;
        }
    }
}