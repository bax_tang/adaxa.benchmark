﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaFilesManager
    {
        private readonly AdaxaClient _client;

        internal AdaxaFilesManager(AdaxaClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<List<PreSignedUrlDto>> CreatePreSignedUrlsAsync(CreatePreSignedUrlsDto request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = AdaxaEndpoints.Files.PreSignedUrls;

            return await _client.PostAsync<CreatePreSignedUrlsDto, List<PreSignedUrlDto>>(requestUri, request, cancellationToken);
        }

        public async Task<PhotoDetailsDto> UploadAsync(PreSignedUrlDto fileInfo, string contentType, Stream fileStream, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestContent = new StreamContent(fileStream);

            requestContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);

            var requestMessage = new HttpRequestMessage(HttpMethod.Put, fileInfo.PreSignedUrl)
            {
                Content = requestContent
            };

            await _client.SendRawRequestAsync(requestMessage, cancellationToken);

            return new PhotoDetailsDto
            {
                Id = fileInfo.Id,
                ObjectKey = fileInfo.ObjectKey
            };
        }
    }
}