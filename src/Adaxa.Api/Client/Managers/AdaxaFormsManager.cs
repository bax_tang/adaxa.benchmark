﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaFormsManager
    {
        private readonly AdaxaClient _client;

        internal AdaxaFormsManager(AdaxaClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<PageDto<FormDetailsDto>> GetListAsync(GetFormsListDto query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(AdaxaEndpoints.Forms.GetList, query);

            return await _client.GetAsync<PageDto<FormDetailsDto>>(requestUri, cancellationToken);
        }

        public async Task<PageDto<FormDetailsDto>> GetMobileListAsync(PaginatedRequestDto query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(AdaxaEndpoints.Forms.GetListMobile, query);

            return await _client.GetAsync<PageDto<FormDetailsDto>>(requestUri, cancellationToken);
        }

        public async Task<FormDetailsWithQuestionsDto> GetAsync(Guid formId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = string.Format(AdaxaEndpoints.Forms.GetById, formId);

            return await _client.GetAsync<FormDetailsWithQuestionsDto>(requestUri, cancellationToken);
        }

        public async Task<FormDetailsWithQuestionsDto> GetMobileAsync(Guid formId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = string.Format(AdaxaEndpoints.Forms.GetByIdMobile, formId);

            return await _client.GetAsync<FormDetailsWithQuestionsDto>(requestUri, cancellationToken);
        }

        public async Task<FormDetailsWithQuestionsDto> CreateAsync(CreateFormDto formDto, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = AdaxaEndpoints.Forms.Create;

            return await _client.PostAsync<CreateFormDto, FormDetailsWithQuestionsDto>(requestUri, formDto, cancellationToken);
        }

        public async Task<FormDetailsWithQuestionsDto> CloneAsDraftAsync(Guid formId, string formName, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(string.Format(AdaxaEndpoints.Forms.CloneAsDraft, formId), new { FormName = formName });

            return await _client.PostAsync<FormDetailsWithQuestionsDto>(requestUri, cancellationToken);
        }

        public async Task<FormDetailsWithQuestionsDto> UpdateAsync(UpdateFormDto formDto, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (!_client.IsCurrentUserHasType(UserType.ManagerLevel1))
            {
                throw new InvalidOperationException("Current user does not have 'Manager Level 1' role. Operation cannot be executed.");
            }

            var requestUri = AdaxaEndpoints.Forms.Update;

            return await _client.PutAsync<UpdateFormDto, FormDetailsWithQuestionsDto>(requestUri, formDto, cancellationToken);
        }

        public async Task<FormDetailsWithQuestionsDto> UpdateDraftAsync(UpdateDraftFormDto formDto, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = AdaxaEndpoints.Forms.UpdateDrafts;

            return await _client.PutAsync<UpdateDraftFormDto, FormDetailsWithQuestionsDto>(requestUri, formDto, cancellationToken);
        }

        public async Task<ServerResponseDto> UpdateStatusAsync(Guid formId, UpdateFormStatusDto statusDto, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(string.Format(AdaxaEndpoints.Forms.UpdateStatus, formId), statusDto);

            return await _client.PutAsync(requestUri, cancellationToken);
        }
    }
}