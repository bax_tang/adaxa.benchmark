﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaSettingsManager
    {
        private readonly AdaxaClient _client;

        internal AdaxaSettingsManager(AdaxaClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<List<LanguageDto>> GetLanguagesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = AdaxaEndpoints.Settings.Languages;

            return await _client.GetAsync<List<LanguageDto>>(requestUri, cancellationToken);
        }
    }
}