﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaUsersManager
    {
        private readonly AdaxaClient _client;

        internal AdaxaUsersManager(AdaxaClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<List<SubmissionUserDetailsDto>> GetAuditorsAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = AdaxaEndpoints.Users.GetAuditors;

            return await _client.GetAsync<List<SubmissionUserDetailsDto>>(requestUri, cancellationToken);
        }

        public async Task<PageDto<SubmissionUserDetailsDto>> SearchAsync(SearchUsersDto searchParameters, CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestUri = _client.MakeRequestUri(AdaxaEndpoints.Users.Search, searchParameters);

            return await _client.GetAsync<PageDto<SubmissionUserDetailsDto>>(requestUri, cancellationToken);
        }
    }
}