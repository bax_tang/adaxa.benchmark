﻿using System;
using System.Net;
using System.Net.Http;

namespace Adaxa.Api
{
    public class AdaxaException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public string Content { get; }

        public AdaxaException(HttpStatusCode statusCode, string message, string content) : base(message)
        {
            StatusCode = statusCode;
            Content = content;
        }
    }
}