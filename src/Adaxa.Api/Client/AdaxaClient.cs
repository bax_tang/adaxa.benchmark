﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using Adaxa.Api.Dto;

namespace Adaxa.Api
{
    public class AdaxaClient : IDisposable
    {
        private const string JsonMediaType = "application/json";

        private static readonly JsonSerializerSettings _settings;

        private readonly HttpClient _client;

        public string AccessToken
        {
            get; internal set;
        }

        public string RefreshToken
        {
            get; internal set;
        }

        public ShortUserDetailsDto CurrentUser
        {
            get; internal set;
        }

        public AdaxaAuthManager Auth { get; }

        public AdaxaFacilitiesManager Facilities { get; }

        public AdaxaFilesManager Files { get; }

        public AdaxaFormsManager Forms { get; }

        public AdaxaSettingsManager Settings { get; }

        public AdaxaSurveysManager Surveys { get; }

        public AdaxaUsersManager Users { get; }

        static AdaxaClient()
        {
            _settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Include,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            };
        }

        public AdaxaClient(string baseUrl)
        {
            if (Uri.TryCreate(baseUrl, UriKind.Absolute, out Uri baseAddress))
            {
                _client = new HttpClient
                {
                    BaseAddress = baseAddress
                };
            }
            else
                throw new ArgumentException("Invalid base address.", nameof(baseUrl));

            Auth = new AdaxaAuthManager(this);
            Facilities = new AdaxaFacilitiesManager(this);
            Files = new AdaxaFilesManager(this);
            Forms = new AdaxaFormsManager(this);
            Settings = new AdaxaSettingsManager(this);
            Surveys = new AdaxaSurveysManager(this);
            Users = new AdaxaUsersManager(this);
        }

        public void Dispose()
        {
            AccessToken = null;
            RefreshToken = null;

            _client.Dispose();
        }

        #region Common methods

        internal async Task<TResponse> GetAsync<TResponse>(string requestUri,
            CancellationToken cancellationToken = default(CancellationToken),
            bool authorizeRequest = true)
        {
            CheckAuthorization(authorizeRequest);

            return await SendRequestAsync<TResponse>(HttpMethod.Get, requestUri, authorizeRequest, cancellationToken);
        }

        internal async Task<ServerResponseDto> PostAsync(string requestUri,
            CancellationToken cancellationToken = default(CancellationToken),
            bool authorizeRequest = true)
        {
            CheckAuthorization(authorizeRequest);

            var rawResponse = await SendRawRequestAsync(HttpMethod.Post, requestUri, authorizeRequest, cancellationToken);

            return ServerResponseDto.FromResponseMessage(rawResponse);
        }

        internal async Task<TResponse> PostAsync<TResponse>(string requestUri,
            CancellationToken cancellationToken = default(CancellationToken),
            bool authorizeRequest = true)
        {
            CheckAuthorization(authorizeRequest);

            return await SendRequestAsync<TResponse>(HttpMethod.Post, requestUri, authorizeRequest, cancellationToken);
        }

        internal async Task<TResponse> PostAsync<TRequest, TResponse>(string requestUri, TRequest request,
            CancellationToken cancellationToken = default(CancellationToken),
            bool authorizeRequest = true)
        {
            CheckAuthorization(authorizeRequest);

            return await SendRequestAsync<TRequest, TResponse>(HttpMethod.Post, requestUri, request, authorizeRequest, cancellationToken);
        }

        internal async Task<ServerResponseDto> PutAsync(string requestUri,
            CancellationToken cancellationToken = default(CancellationToken),
            bool authorizeRequest = true)
        {
            CheckAuthorization(authorizeRequest);

            var rawResponse = await SendRawRequestAsync(HttpMethod.Put, requestUri, authorizeRequest, cancellationToken);

            return ServerResponseDto.FromResponseMessage(rawResponse);
        }

        internal async Task<TResponse> PutAsync<TRequest, TResponse>(string requestUri, TRequest request,
            CancellationToken cancellationToken = default(CancellationToken),
            bool authorizeRequest = true)
        {
            CheckAuthorization(authorizeRequest);

            return await SendRequestAsync<TRequest, TResponse>(HttpMethod.Put, requestUri, request, authorizeRequest, cancellationToken);
        }

        internal async Task<HttpResponseMessage> SendRawRequestAsync(HttpRequestMessage requestMessage,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var responseMessage = await _client.SendAsync(requestMessage, cancellationToken);
            if (responseMessage.IsSuccessStatusCode)
            {
                return responseMessage;
            }
            else
                throw await MakeResponseMessageExceptionAsync(responseMessage);
        }

        internal async Task<HttpResponseMessage> SendRawRequestAsync(HttpMethod method, string requestUri, bool authorizeRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestMessage = new HttpRequestMessage(method, requestUri);

            if (authorizeRequest)
            {
                AuthorizeRequest(requestMessage);
            }

            SetClientCurrentTime(requestMessage);

            var responseMessage = await _client.SendAsync(requestMessage, cancellationToken);
            return responseMessage;
        }

        internal async Task<TResponse> SendRequestAsync<TResponse>(HttpMethod method, string requestUri, bool authorizeRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var responseMessage = await SendRawRequestAsync(method, requestUri, authorizeRequest, cancellationToken);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseContent = await responseMessage.Content.ReadAsStringAsync();

                var response = JsonConvert.DeserializeObject<TResponse>(responseContent, _settings);
                return response;
            }
            else
                throw await MakeResponseMessageExceptionAsync(responseMessage);
        }

        internal async Task<HttpResponseMessage> SendRawRequestAsync<TRequest>(HttpMethod method, string requestUri, TRequest request, bool authorizeRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var requestContent = JsonConvert.SerializeObject(request, _settings);
            var requestMessage = new HttpRequestMessage(method, requestUri)
            {
                Content = new StringContent(requestContent, Encoding.UTF8, JsonMediaType)
            };

            if (authorizeRequest)
            {
                AuthorizeRequest(requestMessage);
            }

            SetClientCurrentTime(requestMessage);

            var responseMessage = await _client.SendAsync(requestMessage, cancellationToken);
            return responseMessage;
        }

        internal async Task<TResponse> SendRequestAsync<TRequest, TResponse>(HttpMethod method, string requestUri, TRequest request, bool authorizeRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var responseMessage = await SendRawRequestAsync(method, requestUri, request, authorizeRequest, cancellationToken);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseContent = await responseMessage.Content.ReadAsStringAsync();

                var response = JsonConvert.DeserializeObject<TResponse>(responseContent, _settings);
                return response;
            }
            else
                throw await MakeResponseMessageExceptionAsync(responseMessage);
        }

        internal void Authorize(string accessToken, string refreshToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new ArgumentNullException(nameof(accessToken));
            }

            if (string.IsNullOrEmpty(refreshToken))
            {
                throw new ArgumentNullException(nameof(refreshToken));
            }

            AccessToken = accessToken;
            RefreshToken = refreshToken;
        }

        internal string MakeRequestUri(string baseRequestUri, object parameters)
        {
            var parametersString = ObjectToParametersString(parameters);

            return string.Concat(baseRequestUri, "?", parametersString);
        }

        internal bool IsCurrentUserHasType(UserType userType)
        {
            if (CurrentUser == null)
            {
                throw new ApplicationException("Adaxa client does not authorized");
            }

            return ((CurrentUser.Type & userType) == userType);
        }

        private void CheckAuthorization(bool authorizeRequest)
        {
            if (authorizeRequest && !IsAuthorized())
            {
                throw new ApplicationException("Client is not authorized.");
            }
        }

        private async Task<AdaxaException> MakeResponseMessageExceptionAsync(HttpResponseMessage responseMessage)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            var responseContent = await responseMessage.Content.ReadAsStringAsync();

            var statusCode = responseMessage.StatusCode;
            var errorMessage = string.Format("Exception occured during request: {0} {1}, reason: {2} {3}\r\ncontent: {4}\r\n",
                responseMessage.RequestMessage.Method,
                responseMessage.RequestMessage.RequestUri,
                (int)responseMessage.StatusCode,
                responseMessage.ReasonPhrase,
                responseContent);

            responseMessage.Dispose();

            return new AdaxaException(statusCode, errorMessage, responseContent);
        }

        private string ObjectToParametersString(object parameters)
        {
            var builder = new StringBuilder(32);

            var type = parameters.GetType();
            var properties = type.GetProperties();

            for (int idx = 0; idx < properties.Length; ++idx)
            {
                var property = properties[idx];

                var value = property.GetValue(parameters);
                if (value != null)
                {
                    builder.Append(char.ToLower(property.Name[0]));
                    builder.Append(property.Name.Substring(1));
                    builder.Append('=');

                    if (value is Enum)
                        builder.Append((int)value);
                    else
                        builder.Append(value.ToString());

                    if (idx != properties.Length - 1)
                    {
                        builder.Append('&');
                    }
                }
            }

            return builder.ToString();
        }

        private void AuthorizeRequest(HttpRequestMessage request)
        {
            if (string.IsNullOrEmpty(AccessToken))
            {
                throw new InvalidOperationException("Cannot authorize request.");
            }

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
        }

        private void SetClientCurrentTime(HttpRequestMessage request)
        {
            var nowTime = DateTimeOffset.Now.ToString();

            request.Headers.Add("Client-Current-Time", nowTime);
        }

        private bool IsAuthorized()
        {
            return
                !string.IsNullOrEmpty(AccessToken) &&
                !string.IsNullOrEmpty(RefreshToken);
        }
        #endregion
    }
}