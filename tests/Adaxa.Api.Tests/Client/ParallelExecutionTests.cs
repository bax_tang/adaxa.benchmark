﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Adaxa.Api;
using Adaxa.Api.Dto;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Adaxa.Api.Tests
{
    [TestClass]
    public class ParallelExecutionTests
    {
        private const string BaseUrl = "https://localhost:44341"; //"http://192.168.24.22"; //

        private JsonSerializerSettings _serializerSettings;

        [TestInitialize]
        public void Initialize()
        {
            var serializerSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            serializerSettings.Converters.Add(new StringEnumConverter(new DefaultNamingStrategy()));

            _serializerSettings = serializerSettings;
        }

        [TestCleanup]
        public void Cleanup()
        {

        }

        [TestMethod]
        public async Task IsAuditorInteractionExecutesSuccessfully()
        {
            var rnd = new Random();

            var auditorAccounts = GetAuditorAccounts();
            var facilitySearchStrings = GetFacilitySearchStrings();

            var tasksList = new Task[auditorAccounts.Count];

            var sw = Stopwatch.StartNew();

            for (int idx = 0; idx < auditorAccounts.Count; ++idx)
            {
                var (AccountName, Password) = auditorAccounts[idx];

                var executionTask = ProceedAuditorInteractionAsync(AccountName, Password, facilitySearchStrings, rnd);

                tasksList[idx] = executionTask;
            }

            sw.Stop();

            await Task.WhenAll(tasksList);

            Console.WriteLine("Execution completed; elapsed time for {0} users: {1}",
                auditorAccounts.Count,
                sw.Elapsed);
        }

        [TestMethod]
        public async Task IsManagerInteractionExecutesSuccessfully()
        {
            var rnd = new Random();

            var managerAccounts = GetManagerAccounts();

            var tasksList = new Task[managerAccounts.Count];

            for (int idx = 0; idx < managerAccounts.Count; ++idx)
            {
                var (AccountName, Password) = managerAccounts[idx];

                var executionTask = ProceedManagerInteractionAsync(AccountName, Password, rnd);

                tasksList[idx] = executionTask;
            }

            await Task.WhenAll(tasksList);
        }

        private List<(string AccountName, string Password)> GetAuditorAccounts()
        {
            var auditorAccounts = new List<(string AccountName, string Password)>
            {
                ("sattestflaud@mcna.net", "pwd"),
                ("sattestflaud@mcna.net", "pwd"),
                ("sattestflaud@mcna.net", "pwd")
            };

            return auditorAccounts;
        }

        private List<(string AccountName, string Password)> GetManagerAccounts()
        {
            var auditorAccounts = new List<(string AccountName, string Password)>
            {
                ("sattestflman@mcna.net", "pwd")
            };

            return auditorAccounts;
        }

        private List<string> GetFacilitySearchStrings()
        {
            var searchStrings = new List<string>
            {
                "kerr",
                "aud",
                "seq",
                "dat",
                "new",
                "stop",
                "ocl",
                "step"
            };

            return searchStrings;
        }

        private async Task ProceedAuditorInteractionAsync(string accountName, string password, List<string> facilitySearchStrings, Random rnd)
        {
            await Task.Delay(100 * rnd.Next(5, 10));

            var client = await AdaxaClientFactory.CreateAuditorClientAsync(BaseUrl, accountName, password);

            var getFormsRequest = new PaginatedRequestDto
            {
                Page = 1,
                Size = rnd.Next(5, 10)
            };

            var formsList = await client.Forms.GetMobileListAsync(getFormsRequest);
            if (formsList.Data.Count <= 0)
            {
                Console.WriteLine($"There is no forms for account '{accountName}'");
                return;
            }

            var formIndex = rnd.Next(0, formsList.Data.Count);
            var formDetails = formsList.Data[formIndex];

            LogObjectAsJson("Random selected form:", formDetails);

            var formWithQuestions = await client.Forms.GetMobileAsync(formDetails.Id);

            var searchStringIndex = rnd.Next(0, facilitySearchStrings.Count);
            var searchString = facilitySearchStrings[searchStringIndex];

            var searchFacilitiesRequest = new SearchFacilitiesDto
            {
                Page = 1,
                Size = rnd.Next(5, 20),
                SortingOrder = SortingOrder.Ascending,
                SortingType = FacilitySortingType.ById,
                SearchString = searchString
            };

            var facilitiesList = await client.Facilities.SearchAsync(searchFacilitiesRequest);
            if (facilitiesList.Data.Count <= 0)
            {
                Console.WriteLine($"There is no facilities found for account '{accountName}'");
                return;
            }

            var facilityIndex = rnd.Next(0, facilitiesList.Data.Count);
            var facilityDetails = facilitiesList.Data[facilityIndex];

            LogObjectAsJson("Random selected facility:", facilityDetails);

            var facilityInfo = await client.Facilities.GetByIdAsync(facilityDetails.Id);

            var createSurveyRequest = await GenerateSurveyRequestAsync(client, formWithQuestions, facilityInfo, rnd);

            var newSurvey = await client.Surveys.CreateAsync(createSurveyRequest);
            var reportRegistration = await client.Surveys.CreateAuditorReportAsync(newSurvey.Id);

            LogObjectAsJson("New survey created:", newSurvey);
            LogObjectAsJson("Report registration response:", reportRegistration);

            do
            {
                await Task.Delay(2000);

                try
                {
                    var surveyReport = await client.Surveys.GetAuditorReportAsync(newSurvey.Id);
                    if (surveyReport != null)
                    {
                        LogObjectAsJson("Report that was generated for survey:", surveyReport);
                        break;
                    }
                }
                catch (AdaxaException adaxaExc)
                {
                    switch (adaxaExc.StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                            continue;
                    }
                }
                catch
                {
                    throw;
                }
            }
            while (true);
        }

        private async Task<PhotoDetailsDto> CreateFileAsync(AdaxaClient client, Stream imageStream)
        {
            var contentType = "image/jpeg";

            var urlRequest = new CreatePreSignedUrlsDto
            {
                Files = new List<CreatePreSignedUrlItemDto>
                {
                    new CreatePreSignedUrlItemDto
                    {
                        ContentType = contentType,
                        Type = FileType.Image
                    }
                }
            };

            var preSignedUrlsList = await client.Files.CreatePreSignedUrlsAsync(urlRequest);

            var fileInfo = preSignedUrlsList[0];

            return await client.Files.UploadAsync(fileInfo, contentType, imageStream);
        }

        private async Task<CreateSurveyDto> GenerateSurveyRequestAsync(AdaxaClient client, FormDetailsWithQuestionsDto form, FacilityDetailsDto facility, Random rnd)
        {
            var facilitySignatureImageStream = new MemoryStream(Resources.Images.SignatureImage);
            var facilitySignature = await CreateFileAsync(client, facilitySignatureImageStream);

            var mcnaSignatureImageStream = new MemoryStream(Resources.Images.SignatureImage);
            var mcnaSignature = await CreateFileAsync(client, mcnaSignatureImageStream);

            var nowTime = DateTimeOffset.Now;

            var createSurveyRequest = new CreateSurveyDto
            {
                Comment = "some comment text",
                Date = nowTime,
                Duration = rnd.Next(0, 50),
                FacilityId = facility.Id,
                FormId = form.Id,
                FacilitySignatureId = facilitySignature.Id,
                FacilitySignatureEmail = "facility_signature@mcna.com",
                FacilitySignatureName = "facility_signature",
                McnaSignatureId = mcnaSignature.Id,
                McnaSignatureEmail = "mcna_signature@mcna.com",
                McnaSignatureName = "mcna_signature",
                Score = string.Empty
            };

            var surveyQuestions = new List<CreateSurveyQuestionDto>();

            var directQuestions = form.Questions.Where(x => x.Type != FormQuestionType.Compound);
            foreach (var question in directQuestions)
            {
                var surveyQuestion = new CreateSurveyQuestionDto
                {
                    Id = question.Id,
                    Answer = new CreateSurveyQuestionAnswerDto
                    {
                        Comment = $"some answer comment for question with id '{question.Id}'",
                        Data = new
                        {
                            Value = "some data for answer"
                        },
                        PhotosIds = new List<Guid>()
                    }
                };

                surveyQuestions.Add(surveyQuestion);
            }

            createSurveyRequest.Questions = surveyQuestions;

            return createSurveyRequest;
        }

        private async Task ProceedManagerInteractionAsync(string accountName, string password, Random rnd)
        {
            await Task.Delay(100 * rnd.Next(5, 10));

            var client = await AdaxaClientFactory.CreateManagerClientAsync(BaseUrl, accountName, password);

            var auditors = await client.Users.GetAuditorsAsync();
            Console.WriteLine($"There is {auditors.Count} auditors linked for current user.");

            var getSurveysRequest = new GetManagerSurveysRequestDto
            {
                Page = 1,
                Size = rnd.Next(5, 20)
            };

            var surveys = await client.Surveys.GetListByManagerAsync(getSurveysRequest);

            var surveysMsg = string.Format("There is {0} surveys for current manager; last date is {1}.",
                surveys.Page.PaginationContext.TotalItems,
                (surveys.LastDate != null) ? surveys.LastDate.Value.ToString() : "<null>");
            Console.WriteLine(surveysMsg);

            var getFormsRequest = new GetFormsListDto
            {
                FormStatus = FormStatus.Draft,
                Page = 1,
                Size = rnd.Next(5, 20)
            };

            var formsList = await client.Forms.GetListAsync(getFormsRequest);
            if (formsList.Data.Count <= 0)
            {
                Console.WriteLine("There is no forms for current manager.");
                return;
            }

            var formsTotalCount = formsList.PaginationContext.TotalItems;

            var formIndex = rnd.Next(0, formsList.Data.Count);
            var formInfo = formsList.Data[formIndex];

            Console.WriteLine($"Selected existing form with Id = '{formInfo.Id}'.");

            var fullFormInfo = await client.Forms.GetAsync(formInfo.Id);

            var updateFormDto = MakeUpdateDraftFormDtoFromDetails(fullFormInfo, "NewName100500");
            var updateResult = await client.Forms.UpdateDraftAsync(updateFormDto);
            if (updateResult != null)
            {
                Console.WriteLine("Existing form updated successfully.");
            }

            var createFormDto = MakeCreateFormDto();
            var newForm = await client.Forms.CreateAsync(createFormDto);
            if (newForm != null)
            {
                Console.WriteLine($"New form created successfully; form id is '{newForm.Id}'.");
            }

            var getUpdatedFormsListRequest = new GetFormsListDto
            {
                FormStatus = FormStatus.Draft,
                Page = 1,
                Size = formsTotalCount + 1
            };

            var updatedFormsList = await client.Forms.GetListAsync(getUpdatedFormsListRequest);
            if (updatedFormsList.Data.Any(x => x.Id == newForm.Id))
            {
                Console.WriteLine("New form correctly created and returns using GET /api/v1/forms endpoint.\r\n");
            }

            client.Dispose();
        }

        private UpdateDraftFormDto MakeUpdateDraftFormDtoFromDetails(FormDetailsWithQuestionsDto formDto, string newName)
        {
            var result = new UpdateDraftFormDto
            {
                Id = formDto.Id,
                Name = string.Concat(newName, "; old name: ", formDto.Name),
                Type = formDto.Type
            };

            var updateQuestions = new List<UpdateDraftFormQuestionDto>(formDto.Questions.Count);

            foreach (var question in formDto.Questions)
            {
                var updateQuestion = new UpdateDraftFormQuestionDto
                {
                    Id = question.Id,
                    CompoundQuestionId = question.CompoundQuestionId,
                    Data = question.Data,
                    IsPhotoAvailable = question.IsPhotoAvailable,
                    Order = question.Order,
                    RenderAnswerClientId = question.RenderCondition?.AnswerId,
                    RenderQuestionId = question.RenderCondition?.QuestionId,
                    Title = question.Title,
                    Type = question.Type
                };

                updateQuestions.Add(updateQuestion);
            }

            result.Questions = updateQuestions;

            return result;
        }

        private CreateFormDto MakeCreateFormDto()
        {
            var formName = "NewFormName_" + Guid.NewGuid().ToString("D").ToUpperInvariant();

            var createFormDto = new CreateFormDto
            {
                Name = formName,
                Type = FormType.SiteAuditTool
            };

            var questions = new List<CreateFormQuestionDto>();

            for (var idx = 0; idx < 5; ++idx)
            {
                var formQuestion = new CreateFormQuestionDto
                {
                    Id = Guid.NewGuid(),
                    Title = "NewFormQuestion_" + idx,
                    Order = idx,
                    Data = new object(),
                    IsPhotoAvailable = false,
                    Type = FormQuestionType.TextInput
                };

                questions.Add(formQuestion);
            }

            createFormDto.Questions = questions;

            return createFormDto;
        }

        private void LogObjectAsJson<TInstance>(string prefix, TInstance instance)
        {
            var instanceJson = JsonConvert.SerializeObject(instance, _serializerSettings);

            Console.WriteLine(string.Concat(prefix, "\r\n", instanceJson));
        }
    }
}