﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;

using Adaxa.Api.Dto;

namespace Adaxa.Api.Tests
{
    [TestClass]
    public class AdaxaClientTests
    {
        private const string ClientBaseUrl = "https://localhost:44341";

        #region Initialize / cleanup methods

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
        }
        #endregion

        #region Test methods

        [TestMethod]
        public async Task IsClientAuthorizationExecutesSuccessfully()
        {
            var client = await AdaxaClientFactory.CreateAuditorClientAsync(ClientBaseUrl);

            Assert.IsNotNull(client.CurrentUser);
            Assert.IsNotNull(client.AccessToken);
            Assert.IsNotNull(client.RefreshToken);

            Console.WriteLine(JsonConvert.SerializeObject(client.CurrentUser, Formatting.Indented));
        }

        [TestMethod]
        public async Task IsFacilitiesSearchExecutesSuccessfully()
        {
            var client = await AdaxaClientFactory.CreateAuditorClientAsync(ClientBaseUrl);

            var searchParams = new SearchFacilitiesDto
            {
                SearchString = "kerr"
            };

            var searchResult = await client.Facilities.SearchAsync(searchParams);

            Console.WriteLine(JsonConvert.SerializeObject(searchResult, Formatting.Indented));
        }

        [TestMethod]
        public async Task IsFormsListExecutesSuccessfully()
        {
            var client = await AdaxaClientFactory.CreateManagerClientAsync(ClientBaseUrl);

            var getFormsListRequest = new GetFormsListDto
            {
                Page = 1,
                Size = 50
            };

            var forms = await client.Forms.GetListAsync(getFormsListRequest);

            Assert.IsNotNull(forms);
            Assert.IsTrue(forms.PaginationContext.TotalItems >= 0);

            Console.WriteLine(JsonConvert.SerializeObject(forms, Formatting.Indented));
        }

        [TestMethod]
        public async Task IsSurveysListExecutesSuccessfully()
        {
            var client = await AdaxaClientFactory.CreateAuditorClientAsync(ClientBaseUrl);

            var getListRequest = new GetAuditorSurveysRequestDto
            {
                Page = 1,
                Size = 50
            };

            var surveys = await client.Surveys.GetListByAuditorAsync(getListRequest);

            Assert.IsNotNull(surveys);
            Assert.IsTrue(surveys.Page.PaginationContext.TotalItems >= 0);

            Console.WriteLine(JsonConvert.SerializeObject(surveys, Formatting.Indented));
        }

        [TestMethod]
        public async Task IsUsersSearchExecutesSuccessfully()
        {
            var client = await AdaxaClientFactory.CreateManagerClientAsync(ClientBaseUrl);

            var searchParams = new SearchUsersDto
            {
                SearchString = "sat"
            };

            var searchResult = await client.Users.SearchAsync(searchParams);

            Console.WriteLine(JsonConvert.SerializeObject(searchResult, Formatting.Indented));
        }
        #endregion
    }
}